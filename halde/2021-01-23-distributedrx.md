---
title: Distributed Rx
publishdate: 2025-01-10
tags: [project, distributedrx, d10rx]
---

# Distributed Rx

I have been [listening](https://hanselminutes.com/771/exploring-event-modeling-with-adam-dymitruk), [watching](https://learn.particular.net/courses/adsd-online) and [reading](https://www.learnrxjs.io/) about Rx and event driven systems and system designs. One thing that struck me odd is that they seem very similiar ideas, but Rx is a concept for coding inside components and events are more an architectural pattern. I am going to try to combine the two. If successful, it should be possible to describe a distributed system with Rx patterns, or distributedrx, d10rx for short.

Here are some principles I'd like to apply:

- Operators (aka slices in event modeling) should be implementable in many languages

As a starter example, I'm trying to implement the following piece of [rxjs code](https://github.com/gregorybleiker/rxjs-vr492e), but in a distributed manor:

```typescript
import { Observable } from "rxjs";
import { take } from "rxjs/operators";

let origin = Observable.create((x) => x.next(100));
let processor = origin.pipe(take(1));
processor.subscribe((x) => console.log(x));
```

There are two parts I want to distribute here: The lambda

```typescript
(x) => next(100);
```

and the

```typescript
(x) => console.log(x);
```

should both be independant code. To make sure this is so, they should also be implemented in different programming languages. To start off though I will start with languages I'm familiar with, [C#](https://docs.microsoft.com/en-us/dotnet/csharp/) and [TypeScript](https://www.typescriptlang.org/). For later I plan to use [Go](https://golang.org/) and [Rust](https://www.rust-lang.org/) to make sure it also works with many different ecosystems.

Even though I would like the system to be completely distributed, the messaging (eventing infrastructure) will be centralized at the moment. I will be using [RabbitMQ](https://www.rabbitmq.com/). If I ever come across a distributed messaging system (like ipfs for files) I'll try to use that instead.

Because I don't want to be bound by the adapters to the messaging system directly, I'll be using gRPC to communicate to the blocks of code.

The first part will be creating the glue code that communicates with RabbitMQ in C#.

On to [Part One]({{< relref "2021-01-23-creatingGlueCode.md" >}})
